#include "../storage/disk_manager.h"

#include <assert.h>    // for assert
#include <string.h>    // for memset
#include <sys/stat.h>  // for stat
#include <unistd.h>    // for lseek
//#include <algorithm>
#include "../defs.h"
#include "disk_manager.h"

//DiskManager负责数据库中页面的分配和回收。它执行对磁盘的页面读写，在数据库管理系统的上下文中提供一个逻辑文件层。

DiskManager::DiskManager() { memset(fd2pageno_, 0, MAX_FD * (sizeof(std::atomic<page_id_t>) / sizeof(char))); }

/**
 * @brief 将buffer中的页面数据写回diskFile中
 *
 */
//先确定从哪开始写
void DiskManager::write_page(int fd, page_id_t page_no, const char *offset, int num_bytes) {
    // Todo:
    // 1.lseek()定位到文件头，通过(fd,page_no)可以定位指定页面及其在磁盘文件中的偏移量
    // 2.调用write()函数
    // 注意处理异常
    //off_t文件偏移量 page_no页面编号
    off_t offset_in_file = static_cast<off_t>(page_no) * PAGE_SIZE; //static_cast类型转换
    //Linux 中可使用系统函数 lseek 来修改文件偏移量 ( 读写位置 )
    if (lseek(fd, offset_in_file, SEEK_SET) == -1) {//读写偏移量将指向 offset_in_file 字节位置处
        throw UnixError();
    }
    ssize_t num_written = write(fd, offset, num_bytes);//对打开的文件进行写操作
    if (num_written == -1) {
        throw UnixError();
    }
    if (num_written != num_bytes) {
        throw UnixError();
    }
}

/**
 * @brief 读取指定编号的页面部分字节到buffer中
 * @param fd 页面所在文件开启后的文件描述符
 * @param page_no 指定页面编号
 * @param offset 读取的内容写入buffer
 * @param num_bytes 读取的字节数
 */
void DiskManager::read_page(int fd, page_id_t page_no, char *offset, int num_bytes) {
    // Todo:
    // 1.lseek()定位到文件头，通过(fd,page_no)可以定位指定页面及其在磁盘文件中的偏移量
    // 2.调用read()函数
    // 注意处理异常
    off_t offset_in_file = static_cast<off_t>(page_no) * PAGE_SIZE;//定位指定页面及其在磁盘文件中的偏移量
    if (lseek(fd, offset_in_file, SEEK_SET) == -1) {//lseek()定位到文件头
        throw UnixError();
    }
    if (read(fd, offset, num_bytes) == -1) {//调用read()函数
        throw UnixError();
    }
}

/**
 * @brief 在磁盘上分配一个页面，只要保持一个递增的计数器
 */
page_id_t DiskManager::allocate_Page(int fd) {
    // Todo:
    // 简单的自增分配策略，指定文件的页面编号加1
    //page_id_t 页ID，磁盘中
    // 对指定文件分配页面编号
    page_id_t newpageno = fd2pageno_[fd];//page id type，分配页面编号
    fd2pageno_[fd]++;//fd为文件描述符
    return newpageno;
}

/**
 * @brief Deallocate page (operations like drop index/table)
 * Need bitmap in header page for tracking pages
 * This does not actually need to do anything for now.
 */
void DiskManager::deallocate_page(__attribute__((unused)) page_id_t page_id) {}

bool DiskManager::is_dir(const std::string &path) {
    struct stat st;
    return stat(path.c_str(), &st) == 0 && S_ISDIR(st.st_mode);
}

void DiskManager::create_dir(const std::string &path) {
    // Create a subdirectory
    std::string cmd = "mkdir " + path;
    if (system(cmd.c_str()) < 0) {  // 创建一个名为path的目录
        throw UnixError();
    }
}

void DiskManager::destroy_dir(const std::string &path) {
    std::string cmd = "rm -r " + path;
    if (system(cmd.c_str()) < 0) {
        throw UnixError();
    }
}

/**
 * @brief 用于判断指定路径文件是否存在 
 * //提示：用struct stat获取文件信息。
 */
bool DiskManager::is_file(const std::string &path) {
    // Todo:
    //struct stat这个结构体是用来描述一个linux系统文件系统中的文件属性的结构。
    //​ 函数原型：int stat(const char *path, struct stat *buf)
    struct stat st;// 用struct stat获取文件信息
    return stat(path.c_str(), &st) == 0;//stat描述文件属性
    //文件名.c_str是string类的一个函数，可以把string类型变量转换成char*变量
}

/**
 * @brief 用于创建指定路径文件
 */
void DiskManager::create_file(const std::string &path) {
    // Todo:
    // 调用open()函数，使用O_CREAT模式，O_CREAT 如果指定文件不存在，则创建这个文件
    // 注意不能重复创建相同文件
    if(is_file(path)) {
        throw FileExistsError(path);
    }
    //int open(const char *pathname, int flags, mode_t mode);
    //pathname参数是要打开或创建的文件的路径名，flags参数用于指定打开文件的方式和属性，mode参数仅在创建新文件时使用，用于指定文件的权限。
    //mode参数是一个八进制数，其中每一位分别指定了对应的读取、写入和执行权限。 例如，0666表示所有用户都可以读、写该文件。
    int fd = open(path.c_str(), O_CREAT, 0666);//调用open()函数，使用O_CREAT模式，O_CREAT 如果指定文件不存在，则创建这个文件
    if(fd == -1) {
        throw UnixError();
    }
    close(fd);
}

/**
 * @brief 用于删除指定路径文件 
 */
void DiskManager::destroy_file(const std::string &path) {
    // Todo:
    // 调用unlink()函数，int unlink(const char * pathname);unlink()会删除参数pathname 指定的文件
    // 注意不能删除未关闭的文件,不存在的文件也不能删
    if (!is_file(path)) {//不能删除不存在的文件
            throw FileNotFoundError(path);
    }
    if (path2fd_.count(path)) {//不能删除未关闭的文件，count表示有线程正在使用该文件
            throw FileNotClosedError(path);
    }
    if (unlink(path.c_str()) == -1) {//调用unlink()函数
        throw UnixError();
    }
}

/**
 * @brief 用于打开指定路径文件
 */
int DiskManager::open_file(const std::string &path) {
    // Todo:
    // 调用open()函数，使用O_RDWR模式
    // 注意不能重复打开相同文件，并且需要更新文件打开列表
    if (path2fd_.count(path)) {
        return -1;
    }//文件已打开，不能重复打开相同文件

    // 使用 O_RDWR 模式打开文件
    int fd = open(path.c_str(), O_RDWR);//O_RDWR表示读、写打开文件
    if (fd < 0) {
        return -1;  // 文件打开失败
    }
    // 文件打开列表，用于记录文件是否被打开，disk_manage.h
    // 更新文件打开列表
    path2fd_.emplace(path,fd);
    fd2path_.emplace(fd,path);

    return fd;
}

/**
 * @brief 用于关闭指定路径文件
 */
void DiskManager::close_file(int fd) {
    // Todo:
    // 调用close()函数
    // 注意不能关闭未打开的文件，并且需要更新文件打开列表

    if (!fd2path_.count(fd)) {
            throw FileNotOpenError(fd);
    }  // 文件未打开

    if(close(fd) == -1){
        throw UnixError(); // 文件关闭失败
    }

    //更新文件打开列表
    auto it1 = path2fd_.find(get_file_name(fd));
    auto it2 = fd2path_.find(fd);
    path2fd_.erase(it1);
    fd2path_.erase(it2);
}



int DiskManager::get_file_size(const std::string &file_name) {
    struct stat stat_buf;
    int rc = stat(file_name.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

std::string DiskManager::get_file_name(int fd) {
    if (!fd2path_.count(fd)) {
        throw FileNotOpenError(fd);
    }
    return fd2path_[fd];
}

int DiskManager::get_file_fd(const std::string &file_name) {
    if (!path2fd_.count(file_name)) {
        return open_file(file_name);
    }
    return path2fd_[file_name];
}


int DiskManager::read_log(char *log_data, int size, int offset, int prev_log_end) {
    // read log file from the previous end
    if (log_fd_ == -1) {
        log_fd_ = open_file(LOG_FILE_NAME);
    }
    offset += prev_log_end;
    int file_size = get_file_size(LOG_FILE_NAME);
    if (offset >= file_size) {
        return false;
    }

    size = std::min(size, file_size - offset);
    lseek(log_fd_, offset, SEEK_SET);
    ssize_t bytes_read = read(log_fd_, log_data, size);
    if (bytes_read != size) {
        throw UnixError();
    }
    return true;
}


void DiskManager::write_log(char *log_data, int size) {
    if (log_fd_ == -1) {
        log_fd_ = open_file(LOG_FILE_NAME);
    }

    // write from the file_end
    lseek(log_fd_, 0, SEEK_END);
    ssize_t bytes_write = write(log_fd_, log_data, size);
    if (bytes_write != size) {
        throw UnixError();
    }
}